﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class UIManager : MonoBehaviour
{
    public static UIManager instance = null; // Экземпляр объекта

    public GameObject mainMenu;//главное меню
    public GameObject shellPanel;//панель вывода кол-ва снарядов
    public GameObject eventPanel;//панель для сообщения
    public Color currentLevelColor;
    public Color nextLevelColor;
    public GameObject levelPanel;
    public Text lvlCurrText;
    public Text lvlNextText;
    public List<Image> listSubLvl = new List<Image>();

    private GameManager gm;
    private Text shellCount;
    
    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance == this)
        {
            Destroy(gameObject);
        }

        DontDestroyOnLoad(gameObject);
    }
    // Start is called before the first frame update
    void Start()
    {
        gm = GameManager.instance;
        shellCount = shellPanel.transform.Find("ShellCount").GetComponent<Text>();
        ShowMainMenu();
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SetNextLevel(int number)
    {
        lvlCurrText.text = number.ToString();
        lvlNextText.text = (number+1).ToString();
        for (int i = 1; i< listSubLvl.Count; i++)
        {
            listSubLvl[i].color = nextLevelColor;
        }
    }
    public void SetNextSubLevel(int number)
    {
        listSubLvl[number].color = currentLevelColor;
    }

    public void ShowMainMenu()
    {
        mainMenu.SetActive(true);
        shellPanel.SetActive(false);
        eventPanel.SetActive(false);
        levelPanel.SetActive(false);
    }
    
    public void SetShellCount(int value)
    {
        shellCount.text = value.ToString();
    }

    public void OnStartBtn()
    {
        gm.StartLevel(0);
        shellPanel.SetActive(true);
        mainMenu.SetActive(false);
        levelPanel.SetActive(true);
    }
    public void OnShowEventPanel(string message)
    {
        Transform tr = eventPanel.transform.Find("MessageText");
        if (tr!=null)
        {
            tr.GetComponent<Text>().text = message;
        }
        eventPanel.SetActive(true);
    }

    public void OnExitBtn()
    {
        Application.Quit();
    }
}
