﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Level", menuName = "ScriptableObjects/LevelScriptableObject", order = 1)]
public class LevelScript : ScriptableObject
{
    public int number;//номер уровня
    public List<GameObject> presetBoxs;

    public int CountSubLevel
    {
        get
        {
            return presetBoxs.Count;
        }
    }
    
}
