﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlLine : MonoBehaviour
{
    
    private GameManager gm;    

    private void Start()
    {
        gm = GameManager.instance;        
    }
    

    private void OnTriggerExit(Collider other)
    {
        if (gm)
        {
            if (other.CompareTag("Box"))
            {
                other.GetComponent<Box>().isStayTrigger = false;
            }
        }        
    }

}
