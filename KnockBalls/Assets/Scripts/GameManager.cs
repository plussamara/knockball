﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public static GameManager instance = null; // Экземпляр объекта

    public GameObject prefShell;//prefab снаряда
    public GameObject startShellPos;//позиция создания снярядов
    public Transform respawnPoint; //позиция установки преграды
    public ControlLine controlLine;

    public List<LevelScript> levelS = new List<LevelScript>();

    public float shellForce = 10;
    [HideInInspector]
    public LevelScript currentLevel;//текущий уровень

    private Plane plane = new Plane(Vector3.forward, 0);//    
    
    private int currentSubLevel = 0;//текущий подуровень
    
    private AudioSource shootAudio;
    private bool startGame = false;
    private int shellCount = 0;//количество патронов

    private string youLose = "You lose";
    private string youWin = "You Win";
    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance == this)
        {
            Destroy(gameObject);
        }

        DontDestroyOnLoad(gameObject);
    }
    // Start is called before the first frame update
    void Start()
    {
        shootAudio = GetComponent<AudioSource>();

        if (prefShell == null)
        {
            Debug.LogError("Не задан префаб для снаряда!");
        }

        if (startShellPos == null)
        {
            Debug.LogError("Не задана стартовая позиция для снаряда!");
        }
        
    }
    
    IEnumerator StartSubLevel(int sublevel)
    {        
        startGame = false;
        yield return new WaitForSeconds(1f);
        UIManager.instance.SetNextSubLevel(sublevel);
        startGame = true;
        shellCount = currentLevel.number * 3;
        UIManager.instance.SetShellCount(shellCount);
        ClearLevel();
        startGame = true;
        GameObject preset = Instantiate(currentLevel.presetBoxs[sublevel], respawnPoint);
        
    }

    IEnumerator CheckGameOver()
    {
        yield return new WaitForSeconds(1.5f);
        //навсякий случай проверяем, вдруг все упало
        if (!CheckNextLevel())
        {
            UIManager.instance.OnShowEventPanel(youLose);
        }
        else
        {
            SubLevelFinish();
        }
    }
    private bool CheckNextLevel()
    {
        Box[] boxes = respawnPoint.GetComponentsInChildren<Box>();
        bool nextLevel = true;
        foreach (Box b in boxes)
        {
            if (b.isStayTrigger)
            {
                nextLevel = false;
                break;
            }
        }
        return nextLevel;
    }
    // Update is called once per frame
    void Update()
    {
        if (!startGame)
            return;

        if (Input.GetKey(KeyCode.Escape))
        {
            startGame = false;
            UIManager.instance.ShowMainMenu();
        }
        if (CheckNextLevel())
        {
            SubLevelFinish();
        }
        else
        {
            if (shellCount == 0)
            {
                startGame = false;
                StartCoroutine(CheckGameOver());
            }
        }
#if UNITY_EDITOR
        if (Input.GetMouseButtonDown(0))
        {            
            Fire(Input.mousePosition);
        }

#else
        if (Input.touchCount > 0)
        {
            Touch touch = Input.GetTouch(0);
            if (touch.phase == TouchPhase.Began)
            {
                Fire(touch.position);
            }
        }
#endif
    }

    void Fire(Vector3 pointPos)
    {
        
        float dist;
        Vector3 pointTarget;
        Ray ray = Camera.main.ScreenPointToRay(pointPos);
        if (plane.Raycast(ray,out dist))
        {
            pointTarget = ray.GetPoint(dist);
            Vector3 dir = pointTarget - startShellPos.transform.position;
            GameObject shell = Instantiate(prefShell, startShellPos.transform);

            Rigidbody rg = shell.GetComponent<Rigidbody>();
            if (rg != null)
            {
                rg.AddForce(dir * shellForce, ForceMode.Impulse);
            }
            shootAudio.Play();
            shellCount--;
            if (shellCount < 0)
                shellCount = 0;
            UIManager.instance.SetShellCount(shellCount);
        }        
    }
    //чистим уровень
    void ClearLevel()
    {
        int count = startShellPos.transform.childCount;
        for (int i=0; i<count; i++)
        {
            Destroy(startShellPos.transform.GetChild(i).gameObject);
        }
        count = respawnPoint.childCount;
        for (int i = 0; i < count; i++)
        {
            Destroy(respawnPoint.GetChild(i).gameObject);
        }
    }

    //запускаем уровень из списка
    public void StartLevel(int level)
    {
        startGame = true;
        currentSubLevel = 0;
        currentLevel = levelS[level];
        ClearLevel();
        UIManager.instance.SetNextLevel(currentLevel.number);
        if (currentLevel.CountSubLevel > 0)
        {
            StartCoroutine(StartSubLevel(0));
        }
    }
    private void SubLevelFinish()
    {
        currentSubLevel++;
        if (currentSubLevel < currentLevel.presetBoxs.Count)
        {
            StartCoroutine(StartSubLevel(currentSubLevel));
        }
        else
        {
            int nextLevel = currentLevel.number;
            if (nextLevel<levelS.Count)
            {
                StartLevel(nextLevel);
            }
            else
            {
                startGame = false;
                UIManager.instance.OnShowEventPanel(youWin);
            }
        }
    }
}
